package com.indix.sqlqueryengine

/**
  * Created by Saurabh Agrawal.
  */
class SqlQueryEngine {

  /**
    * This method executes the query entered by user.
    * @param query Query entered by user.
    * @return Result of executing the query entered by user.
    */
  def execute(query: String): List[String] = {
    val dataset = new DatasetLoader().load
    val (selectStatement, whereClause) = parse(query)

    validateSelectStatement(selectStatement)

    val filteredDataset = whereClause match {
      case "" => dataset
      case _ => new WhereClauseEvaluator().evaluate(dataset, whereClause)
    }
    new SelectStatementEvaluator().evaluate(filteredDataset, selectStatement)
  }

  /**
    * Parse user query and returns tuple of "select statement" and "where clause".
    * Assuming "FROM" to be constant.
    * @param query Query entered by user.
    * @return Tuple of "select statement" and "where clause".
    */
  private def parse(query: String): (String, String) = {
    val selectPattern = "SELECT\\s+(.*)\\s+FROM.*".r
    val wherePattern = ".*WHERE\\s+(.*)".r

    val selectStatement = query.toUpperCase match {
      case selectPattern(group) => group
      case _ => ""
    }

    val whereClause = query.toUpperCase match {
      case wherePattern(group) => group
      case _ => ""
    }

    (selectStatement.trim, whereClause.trim)
  }

  /**
    * This method validates select statement returned by parse() method. If empty, it prints an error and stops execution.
    * @param selectStatement Select statement as returned by parse() method after parsing the query.
    */
  private def validateSelectStatement(selectStatement: String): Unit = {
    if (selectStatement == "") {
      println("Error: Unable to parse query.")
      System.exit(1)
    }
  }
}
