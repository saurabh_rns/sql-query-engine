package com.indix.sqlqueryengine

/**
  * Created by Saurabh Agrawal.
  */
class SqlFunctionEvaluator {

  /**
    * This method evaluates the function such as UNIQ or MAX specified in select statement of query.
    * @param dataset Dataset
    * @param fields Field specified in select statement.
    * @return Result after evaluating the function.
    */
  def evaluate(dataset: List[Product], fields: List[String]): List[String] = {
    val function = getFunction(fields)
    validateFunction(function)
    val field = getField(fields)
    validateField(field)
    val result = evaluate(dataset, function, field)
    result
  }

  /**
    * This method extracts function name from select statement.
    * @param fields Function applied on field specified in select statement.
    * @return Function name
    */
  def getFunction(fields: List[String]): String = {
    val functionPattern = "(.*)[(][_a-z]+[)]".r

    val function = fields.head match {
      case functionPattern(group) => group
      case _ => ""
    }
    function
  }

  /**
    * This method validates the function name.
    * If invalid function name it prints error message and stops execution.
    * @param function Function name
    */
  private def validateFunction(function: String): Unit = {
    val functions = List("max", "uniq")
    if(!functions.contains(function)) {
      println("Error: Invalid SQL function specified in SELECT statement.")
      System.exit(1)
    }
  }

  /**
    * This method extracts field on which the function is applied.
    * @param fields Function applied on field specified in select statement.
    * @return field on which function is applied.
    */
  def getField(fields: List[String]): String = {
    val fieldPattern = ".*[(](.*)[)]".r

    val field = fields.head match {
      case fieldPattern(group) => group
      case _ => ""
    }
    field
  }

  /**
    * This method validates the field on which function is to be applied.
    * If invalid field it prints error message and stops execution.
    * @param field Function name
    */
  private def validateField(field: String): Unit = {
    val fields = List("title", "brand", "store", "price", "in_stock")
    if(!fields.contains(field)) {
      println("Error: Invalid field specified in SELECT statement.")
      System.exit(1)
    }
  }

  /**
    * This method applies function specified on the field.
    * @param dataset Dataset
    * @param function SQL Function MAX / UNIQ
    * @param field field on which the function is to be applied.
    * @return Result after applying function on field.
    */
  private def evaluate(dataset: List[Product], function: String, field: String): List[String] = {
    function match {
      case "max" => List.empty[String] :+ max(dataset, field)
      case "uniq" => uniq(dataset, field)
    }
  }

  /**
    * This method finds maximum value of the specified field.
    * @param dataset Dataset
    * @param field Field specified in select statement.
    * @return Returns maximum value of the specified field.
    */
  private def max(dataset: List[Product], field: String): String = {
    field match {
      case "title" => dataset.maxBy(_.title).title
      case "brand" => dataset.maxBy(_.brand).brand.toString
      case "store" => dataset.maxBy(_.store).store.toString
      case "price" => dataset.maxBy(_.price).price.toString
      case "in_stock" => dataset.maxBy(_.in_stock).in_stock.toString
    }
  }

  /**
    * This method find unique values of the specified field.
    * @param dataset Dataset
    * @param field Field specified in select statement.
    * @return Returns unique values of the specified field.
    */
  private def uniq(dataset: List[Product], field: String): List[String] = {
    field match {
      case "title" => dataset.map(_.title).distinct
      case "brand" => dataset.map(_.brand.toString).distinct
      case "store" => dataset.map(_.store.toString).distinct
      case "price" => dataset.map(_.price.toString).distinct
      case "in_stock" => dataset.map(_.in_stock.toString).distinct
    }
  }
}