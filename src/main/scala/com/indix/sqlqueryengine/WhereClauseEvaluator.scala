package com.indix.sqlqueryengine

import scala.collection.mutable.Stack

/**
  * Created by Saurabh Agrawal.
  */
class WhereClauseEvaluator {

  var operands = Stack[Any]()
  var operators = Stack[String]()
  val precedence = Map("(" -> 0, "AND" -> 1, "OR" -> 1, "=" -> 2, "!=" -> 2, ">" -> 2, ">=" -> 2, "<" -> 2, "<=" -> 2)
  var data: List[Product] = _

  /**
    * This method evaluates where clause and return filtered dataset accordingly.
    * @param dataset Dataset
    * @param whereClause Where clause of the query entered by user.
    * @return Returns filtered dataset as per the where clause.
    */
  def evaluate(dataset: List[Product], whereClause: String): List[Product] = {
    data = dataset
    val tokens = getTokens(whereClause)
    val value = evaluateTokens(tokens)
    value
  }

  /**
    * This method divides the where clause in different tokens.
    * @param whereClause Where clause of the query entered by user.
    * @return Tokeniezed where clause.
    */
  private def getTokens(whereClause: String): Array[String] = {
    val parantheses = List("(", ")")
    val simpleOperators = List("=", ">", "<")
    val complexOperators = List("!=", ">=", "<=")

    val intermediateExpressionOne = (simpleOperators ++ complexOperators).foldLeft(whereClause) {
      (expression, operator) =>
        expression.replaceAll(s"$operator", s" $operator ")
    }

    val intermediateExpressionTwo = parantheses.foldLeft(intermediateExpressionOne) {
      (expression, operator) =>
        expression.replaceAll(s"[$operator]", s" $operator ")
    }

    complexOperators.foldLeft(intermediateExpressionTwo) {
      (expression, operator) =>
        val operatorComponents = operator.toList
        expression.replaceAll(s"${operatorComponents(0)}\\s+${operatorComponents(1)}", s"${operatorComponents(0)}${operatorComponents(1)}")
    }.trim.split("\\s+").map(_.trim)
  }

  /**
    * This method evaluates the tokens and returns filtered dataset.
    * @param tokens Tokenized where clause.
    * @return Evaluates where clause and returns filtered dataset.
    */
  private def evaluateTokens(tokens: Array[String]): List[Product] = {
    tokens.foreach {
      token =>
        val tokenType = getTokenType(token)
        tokenType match {
          case "operand" =>
            operands.push(token.toLowerCase)
          case "operator" =>
            while (!operators.isEmpty && precedence(operators.top) >= precedence(token.toUpperCase)) process
            operators.push(token.toUpperCase)
          case "left_parenthesis" =>
            operators.push(token)
          case "right_parenthesis" =>
            while (operators.top != "(") process
            operators.pop
        }
    }
    while (!operators.isEmpty) process
    operands.pop.asInstanceOf[List[Product]]
  }

  /**
    * Token type can be:
    * 1. Operator
    * 2. Operand
    * 3. Left parenthesis
    * 4. Right parenthesis
    * @param token Token from where clause
    * @return Token type
    */
  private def getTokenType(token: String): String = {
    val operators = List("=", ">", "<", "!=", ">=", "<=", "OR", "AND")

    if (operators.contains(token.toUpperCase)) "operator"
    else if (token == ")") "right_parenthesis"
    else if (token == "(") "left_parenthesis"
    else "operand"
  }

  private def process: Unit = {
    val operator = operators.pop
    val operand2 = operands.pop
    val operand1 = operands.pop
    val value = evaluateExpression(operand1, operand2, operator)
    operands.push(value)
  }

  private def evaluateExpression(operand1: Any, operand2: Any, operator: String): List[Product] = {
    operator match {
      case "=" | "!=" | ">" | ">=" | "<" | "<=" =>
        operand1.toString match {
          case "title" => handleTitle(operand2.toString, operator)
          case "brand" => handleBrand(operand2.toString.toInt, operator)
          case "store" => handleStore(operand2.toString.toInt, operator)
          case "price" => handlePrice(operand2.toString.toFloat, operator)
          case "in_stock" => handleInStock(if (operand2.toString == "true") true else false, operator)
        }
      case "AND" | "OR" => handleAndOr(operand1.asInstanceOf[List[Product]], operand2.asInstanceOf[List[Product]], operator)
    }
  }

  private def handleTitle(operand: String, operator: String): List[Product] = {
    operator match {
      case "=" => data.filter(_.title == operand)
      case "!=" => data.filter(_.title != operand)
      case ">" => data.filter(_.title > operand)
      case ">=" => data.filter(_.title >= operand)
      case "<" => data.filter(_.title < operand)
      case "<=" => data.filter(_.title <= operand)
    }
  }

  private def handleBrand(operand: Int, operator: String): List[Product] = {
    operator match {
      case "=" => data.filter(_.brand == operand)
      case "!=" => data.filter(_.brand != operand)
      case ">" => data.filter(_.brand > operand)
      case ">=" => data.filter(_.brand >= operand)
      case "<" => data.filter(_.brand < operand)
      case "<=" => data.filter(_.brand <= operand)
    }
  }

  private def handleStore(operand: Int, operator: String): List[Product] = {
    operator match {
      case "=" => data.filter(_.store == operand)
      case "!=" => data.filter(_.store != operand)
      case ">" => data.filter(_.store > operand)
      case ">=" => data.filter(_.store >= operand)
      case "<" => data.filter(_.store < operand)
      case "<=" => data.filter(_.store <= operand)
    }
  }

  private def handlePrice(operand: Float, operator: String): List[Product] = {
    operator match {
      case "=" => data.filter(_.price == operand)
      case "!=" => data.filter(_.price != operand)
      case ">" => data.filter(_.price > operand)
      case ">=" => data.filter(_.price >= operand)
      case "<" => data.filter(_.price < operand)
      case "<=" => data.filter(_.price <= operand)
    }
  }

  private def handleInStock(operand: Boolean, operator: String): List[Product] = {
    operator match {
      case "=" => data.filter(_.in_stock == operand)
      case "!=" => data.filter(_.in_stock != operand)
    }
  }

  private def handleAndOr(operand1: List[Product], operand2: List[Product], operator: String): List[Product] = {
    operator match {
      case "AND" => operand1.toSet.&(operand2.toSet).toList
      case "OR" => operand1.toSet.|(operand2.toSet).toList
    }
  }
}