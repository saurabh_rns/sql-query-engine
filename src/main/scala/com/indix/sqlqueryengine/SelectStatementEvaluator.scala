package com.indix.sqlqueryengine

/**
  * Created by Saurabh Agrawal.
  */
class SelectStatementEvaluator {

  val functionPattern = "[a-z]+[(][_a-z]+[)]"

  /**
    * This method evaluates the select statement specified in query.
    * @param dataset Dataset
    * @param selectStatement Select statement of query entered by user.
    * @return Returns projection of result as per fields specified in select statement.
    */
  def evaluate(dataset: List[Product], selectStatement: String): List[String] = {
    val fields = getFields(selectStatement)

    validateFields(fields)

    val result = if (fields.length == 1 && fields.head.matches(functionPattern)) {
      new SqlFunctionEvaluator().evaluate(dataset, fields)
    } else {
      projectFields(dataset, fields)
    }
    getHeader(fields) +: result
  }

  /**
    * This method splits the select statement in different fields based on comma (",").
    * @param selectStatement Select statement of query entered by user.
    * @return Returns fields specified in select statement.
    */
  private def getFields(selectStatement: String): List[String] = {
    selectStatement.trim.split(",").toList.map(_.toLowerCase.trim)
  }

  /**
    * This method validates fields specified in select statement.
    * If invalid fields are specified, then it prints an error message and stops execution.
    * @param fields Fields specified in select statement.
    */
  private def validateFields(fields: List[String]): Unit = {
    var validation_successful = true
    val columns = List("title", "brand", "store", "price", "in_stock", "*")
    if (fields.length > 1 && fields.contains("[(]")) {
      println("Error: Invalid SELECT statement. Function cannot be specified along with other field.")
      validation_successful = false
    } else if (fields.length > 1 && fields.contains("*")) {
      print("Error: Invalid SELECT statement. * cannot be specified along with other field.")
      validation_successful = false
    } else if (!fields.forall(field => columns.contains(field) || field.matches(functionPattern))) {
      print("Error: Invalid SELECT statement. Invalid field specified.")
      validation_successful = false
    }

    if (!validation_successful) {
      System.exit(1)
    }
  }

  /**
    * This method returns the result of query as per the fields specified in select statement.
    * @param dataset Dataset
    * @param fields Fields specified in select statement.
    * @return Returns the result as per the fields specified.
    */
  private def projectFields(dataset: List[Product], fields: List[String]): List[String] = {
    var result = List.empty[String]
    for (row <- dataset) {
      var projection = List.empty[String]
      for (field <- fields) {
        field match {
          case "title" => projection = projection :+ row.title.toString
          case "brand" => projection = projection :+ row.brand.toString
          case "store" => projection = projection :+ row.store.toString
          case "price" => projection = projection :+ (if (row.price.toInt == row.price) row.price.toInt.toString else row.price.toString)
          case "in_stock" => projection = projection :+ row.in_stock.toString
          case "*" => projection = projection :+ row.toString
        }
      }
      result = result :+ projection.mkString(",")
    }
    result
  }

  /**
    * This method returns header as per the fields specified in select statement.
    * @param fields Fields specified in select statement.
    * @return Returns header as per the fields specified in select statement.
    */
  private def getHeader(fields: List[String]): String = {
    if (fields.contains("*")) {
      "title,brand,store,price,in_stock"
    } else {
      fields.mkString(",")
    }
  }
}