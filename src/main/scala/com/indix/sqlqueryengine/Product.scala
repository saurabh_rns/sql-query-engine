package com.indix.sqlqueryengine

/**
  * Created by Saurabh Agrawal.
  */
case class Product(title: String,
                   brand: Int,
                   store: Int,
                   price: Float,
                   in_stock: Boolean) {

  override def toString: String = {
    s"$title,$brand,$store,${if (price.toInt == price) price.toInt else price},$in_stock"
  }
}