package com.indix.sqlqueryengine

import scala.io.Source

/**
  * Created by Saurabh Agrawal.
  */
class DatasetLoader {

  val file = "sql_engine_dataset.csv"

  /**
    * Reads dataset file and convert each record into Product object and returns list of Product.
    * Parsing records based on the assumption that format of records is constant.
    * @return List of Product
    */
  def load: List[Product] = {
    Source.fromFile(file).getLines.map {
      record =>
        val fields = record.split(",")
        val title = fields(0).trim
        val brand = fields(1).trim.toInt
        val store = fields(2).trim.toInt
        val price = fields(3).trim.toFloat
        val in_stock = if (fields(4).trim.toLowerCase == "true") true else false
        Product(title, brand, store, price, in_stock)
    }.toList
  }
}
