package com.indix.sqlqueryengine

import scala.io.StdIn

/**
  * Created by Saurabh Agrawal.
  */
object Application {
  def main(args: Array[String]): Unit = {
    print("Please enter your query: ")
    val query = StdIn.readLine
    val result = new SqlQueryEngine().execute(query)
    result.foreach(println)
  }
}